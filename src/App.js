import './App.scss';
import ParkingComponent from "./components/ParkingComponent/ParkingComponent.js";

function App() {
  return (
    <div className="App">
      <ParkingComponent />
    </div>
  );
}

export default App;
