export const ADD_VEHICLE_TO_VEHICLE_LIST = 'ADD_VEHICLE_TO_VEHICLE_LIST'
export const ADD_VEHICLE_FROM_PARKING = 'ADD_VEHICLE_FROM_PARKING'
export const REMOVE_VEHICLE_FROM_PARKING = 'REMOVE_VEHICLE_FROM_PARKING'

export function addVehicleToVehicleListAction(vehicle) {
    return {
        type: ADD_VEHICLE_TO_VEHICLE_LIST,
        typeVehicle: vehicle.typeVehicle,
        vehicleId: vehicle.vehicleId
    }
}

export function addVehicleFromParkingAction(vehicle) {
    return {
        type: ADD_VEHICLE_FROM_PARKING,
        typeVehicle: vehicle.typeVehicle,
        vehicleId: vehicle.vehicleId
    }
}

export function removeVehicleFromParkingAction(vehicleId) {
    return {
        type: REMOVE_VEHICLE_FROM_PARKING,
        vehicleId: vehicleId

    }
}


