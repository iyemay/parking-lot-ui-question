import React from 'react';
import { v4 as uuid } from 'uuid';
import { connect } from 'react-redux';
import { addVehicleToVehicleListAction,
         addVehicleFromParkingAction,
} from "../../actions/actions";

class AddVehicleComponent extends React.Component {

    state = {
        vehicleName: ''
    };

    handleChange = (event) => {
        this.setState({
            vehicleName: event.target.value
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        let parkingListLenght = this.props.parkingVehicle.length;
        let newVehicle = {
            vehicleId: uuid(),
            typeVehicle: this.state.vehicleName
        }

        if(parkingListLenght < 3) {
            this.props.addVehicleFromParkingAction(newVehicle);
        } else {
            this.props.addVehicleToVehicleListAction(newVehicle);
        }
        this.setState( {
            vehicleName: ''
        })
    };

    parkVehicle = () => {
        const randPos = Math.floor(Math.random() * this.props.vehicleList.length);
        const randomVehicle = this.props.vehicleList[randPos];
        let parkingListLenght = this.props.parkingVehicle.length;
        console.log(randomVehicle);
        if(parkingListLenght < 3) {
            this.props.addVehicleFromParkingAction(randomVehicle);
        } else {
            alert("El vehículo deberá esperar por un espacio de aparcamiento");
        }
    }

    render() {
        return(
            <div className="d-flex flex-column">
                <div className="d-flex flex-row">
                    <div className="d-flex flex-column">
                        <p className="mb-0">Adicionar nuevo Vehículo</p>
                        <input type="text" onChange={this.handleChange} value={this.state.vehicleName || ''}
                                placeholder="Motocicleta/Sedan/Camión"/>
                    </div>

                    <div className="ml-3 align-self-md-end">
                        <button className="btn btn-success" onClick={this.handleSubmit}>
                            Adicionar Vehículo
                        </button>
                    </div>
                </div>

                <div className="d-flex flex-column mt-5">
                    <div className="d-flex flex-column flex-md-row mb-5 mb-md-3">
                        <button className="btn btn-info ml-3"
                                onClick={this.parkVehicle}>
                            Aparcar Vehículo
                        </button>
                    </div>

                    <p className="queue-style mb-0">Cola de Vehículos:</p>
                    {this.props.vehicleList.map(vehicle => (
                        <div className="d-flex flex-row align-self-center mt-3" key={vehicle.vehicleId}>
                            <p className="mb-0 align-self-center">
                                {vehicle.typeVehicle}
                            </p>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        vehicleList: state.vehicleList,
        parkingVehicle: state.parkingVehicle
    };
}

const mapDispatchToProps = { addVehicleToVehicleListAction,
                             addVehicleFromParkingAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddVehicleComponent);