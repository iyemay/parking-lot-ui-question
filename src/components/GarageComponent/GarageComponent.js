import React from 'react';
import { connect } from 'react-redux'
import { removeVehicleFromParkingAction } from "../../actions/actions";

class GarageComponent extends React.Component {

    removeVehicleOfParking = (vehicleId) => {
        console.log(vehicleId);
        this.props.removeVehicleFromParkingAction(vehicleId);
    }

    render() {
            return(
                <div className="d-flex flex-column mt-5 mt-md-0 align-items-center">
                    <p className="garage-style">Aparcamientos de vehículos</p>
                    <p>Aparcamientos ocupados {this.props.parkingVehicle.length}/3</p>
                    <div className="flex-column">
                        {this.props.parkingVehicle.map(vehicle => (
                            <div className="d-flex flex-row align-self-center mt-3"
                                 key={vehicle.vehicleId}>
                                <p className="mb-0 align-self-center type-vehicle mr-2">
                                    {vehicle.typeVehicle}
                                </p>
                                <div>
                                    <button className="btn btn-danger"
                                            onClick={ () => this.removeVehicleOfParking(vehicle.vehicleId) }>
                                        <i className="fas fa-minus-circle"/>
                                    </button>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
    }
}

const mapStateToProps = (state) => {
    return {
        parkingVehicle: state.parkingVehicle
    };
}

const mapDispatchToProps = {
    removeVehicleFromParkingAction
};

export default connect(mapStateToProps, mapDispatchToProps)(GarageComponent);