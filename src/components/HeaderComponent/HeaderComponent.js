import React from 'react';

const HeaderComponent = () => {
    return(
        <div className="d-flex justify-content-center header-style">
            <p>Parking Lot UI Question</p>
        </div>
    );
}

export default HeaderComponent;