import React from 'react';
import HeaderComponent from '../HeaderComponent/HeaderComponent';
import AddVehicleComponent from '../AddVehicleComponent/AddVehicleComponent';
import GarageComponent from '../GarageComponent/GarageComponent';

const ParkingComponent = () => {
    return(
        <div className="d-flex flex-column w-75 mx-auto mt-5 justify-content-center">
            <HeaderComponent />
            <div className="d-flex flex-column flex-md-row mt-5 justify-content-md-between">
                <AddVehicleComponent />
                <GarageComponent />
            </div>
        </div>
    );
}

export default ParkingComponent;