import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import './components/HeaderComponent/HeaderComponent.scss';
import './components/AddVehicleComponent/AddVehicleComponent.scss';
import './components/GarageComponent/GarageComponent.scss';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import parkingReducer from './reducers/reducer'

const store = createStore(
    parkingReducer,
    applyMiddleware(logger)
);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
