import { ADD_VEHICLE_TO_VEHICLE_LIST,
         ADD_VEHICLE_FROM_PARKING,
         REMOVE_VEHICLE_FROM_PARKING
} from "../actions/actions";

const initialState = {
    vehicleList: [],
    parkingVehicle: [],
};

export default function parkingReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_VEHICLE_TO_VEHICLE_LIST: {
            return  {
                ...state,
                vehicleList: [
                    ...state.vehicleList,
                    {
                        typeVehicle: action.typeVehicle,
                        vehicleId: action.vehicleId
                    }
                ]
            }
        }
        case ADD_VEHICLE_FROM_PARKING: {
            return {
                parkingVehicle: [
                    ...state.parkingVehicle,
                    {
                        typeVehicle: action.typeVehicle,
                        vehicleId: action.vehicleId
                    }
                ],
                vehicleList: [
                    ...state.vehicleList.filter(vehicle => vehicle.vehicleId !== action.vehicleId)
                ]
            }
        }
        case REMOVE_VEHICLE_FROM_PARKING: {
            return {
                ...state,
                parkingVehicle: [
                    ...state.parkingVehicle.filter(vehicle => vehicle.vehicleId !== action.vehicleId)
                ]
            }
        }
        default:
            return initialState;
    }
}